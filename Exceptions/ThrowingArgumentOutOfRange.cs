﻿namespace Exceptions
{
    public static class ThrowingArgumentOutOfRange
    {
        public static bool CheckParameterAndThrowException1(int i)
        {
            // TODO 1-1. Add the code to throw the ArgumentOutOfRangeException if the i argument is outside the allowable range.
            return true;
        }

        public static bool CheckParameterAndThrowException2(ulong l)
        {
            // TODO 1-2. Add the code to throw the ArgumentOutOfRangeException if the l argument is outside the allowable range.
            return true;
        }

        public static bool CheckParametersAndThrowException3(uint i, double d)
        {
            // TODO 1-3. Add the code to throw the ArgumentOutOfRangeException if the i and d arguments are outside the allowable range.
            return true;
        }

        public static bool CheckParametersAndThrowException4(long l, float f)
        {
            // TODO 1-4. Add the code to throw the ArgumentOutOfRangeException if the l and f arguments are outside the allowable range.
            return true;
        }
    }
}
